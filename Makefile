# This mod requires Fennel and is best run with bmake because
# that's what this make file is made for

DESTDIR = ${HOME}/.minetest/mods
SRC = src/main.fnl src/irc.fnl
# post-transpile source code
.PHONY: build 
.MAIN: build
build:
	fennel -c ${SRC} > init.lua
install:
	@( mkdir -p ${DESTDIR}/minebridger ) # who even checks for folders these days :|
	@( cp mod.conf init.lua ${DESTDIR}/minebridger )

clean:
	rm init.lua

nuke:
	rm -rf init.lua ${DESTDIR}/MineBridger
